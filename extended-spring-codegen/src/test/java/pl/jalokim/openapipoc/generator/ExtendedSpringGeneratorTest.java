package pl.jalokim.openapipoc.generator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;
import org.openapitools.codegen.ClientOptInput;
import org.openapitools.codegen.DefaultGenerator;
import org.openapitools.codegen.config.CodegenConfigurator;

/***
 * This test allows you to easily launch your code generation software under a debugger.
 * Then run this test under debug mode.  You will be able to step through your java code 
 * and then see the results in the out directory. 
 *
 * To experiment with debugging your code generator:
 * 1) Set a break point in ExtendedSpringGenerator.java in the postProcessOperationsWithModels() method.
 * 2) To launch this test in Eclipse: right-click | Debug As | JUnit Test
 *
 */
public class ExtendedSpringGeneratorTest {

  // use this test to launch you code generator in the debugger.
  // this allows you to easily set break points in MyclientcodegenGenerator.
  @Test
  public void launchCodeGenerator() throws IOException {
    // given
    List<String> expectedLinesToFound = Files.lines(Path.of("src/test/resources/expectedGeneratedLinePetClass")).collect(Collectors.toList());

    // when
    // to understand how the 'openapi-generator-cli' module is using 'CodegenConfigurator', have a look at the 'Generate' class:
    // https://github.com/OpenAPITools/openapi-generator/blob/master/modules/openapi-generator-cli/src/main/java/org/openapitools/codegen/cmd/Generate.java 
    final CodegenConfigurator configurator = new CodegenConfigurator()
              .setGeneratorName("extended-spring") // this not working somehow
              .setInputSpec("../application-open-api-contract/src/main/resources/api-contract/pet-api.yaml") // sample OpenAPI file
              // .setInputSpec("https://raw.githubusercontent.com/openapitools/openapi-generator/master/modules/openapi-generator/src/test/resources/2_0/petstore.yaml") // or from the server
              .setOutputDir("target/out/extended-spring-generator"); // output directory

    final ClientOptInput clientOptInput = configurator.toClientOptInput();
    DefaultGenerator generator = new DefaultGenerator();
    generator.opts(clientOptInput).generate();

    // then
    Files.lines(Path.of("target/out/extended-spring-generator/src/main/java/org/openapitools/model/Pet.java"))
        .forEach(line-> {
          String foundLine = null;
          for (String expectedToFound : expectedLinesToFound) {
            if (line.equals(expectedToFound)) {
              foundLine = expectedToFound;
              break;
            }
          }
          Optional.ofNullable(foundLine)
              .ifPresent(expectedLinesToFound::remove);
        });

    Assert.assertTrue(expectedLinesToFound.isEmpty());
  }
}
