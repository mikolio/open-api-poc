package pl.jalokim.openapipoc.generator;

import com.google.common.base.CaseFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.openapitools.codegen.CodegenModel;
import org.openapitools.codegen.languages.SpringCodegen;

public class ExtendedSpringGenerator extends SpringCodegen {

    public static final String EXTENDED_SPRING_GENERATOR_NAME = "extended-spring";

    public ExtendedSpringGenerator() {
        super();
        templateDir = EXTENDED_SPRING_GENERATOR_NAME;
    }

    @Override
    public String getName() {
        return EXTENDED_SPRING_GENERATOR_NAME;
    }

    private static final Map<String, Consumer<ModelVendorExtensionData>> ANNOTATION_CREATOR_BY_TAG =
        Map.of(
            "x-field-required-when-other-empty", modelVendorExtensionData -> {
                modelVendorExtensionData.addAnnotation("pl.jalokim.annotations.extgenerator", "FieldRequiredWhenOtherEmpty",
                    List.of(
                        requiredAnnotationArg("x-required-field-name", modelVendorExtensionData),
                        requiredAnnotationArg("x-empty-field-name", modelVendorExtensionData),
                        nonRequiredAnnotationArg("x-message", modelVendorExtensionData)
                    ));
            },
            "x-field-required-when-other-field-has-any-value", modelVendorExtensionData -> {
                modelVendorExtensionData.addAnnotation("pl.jalokim.annotations.extgenerator", "FieldRequiredWhenOtherFieldHasAnyValue",
                    List.of(
                        requiredAnnotationArg("x-required-field-name", modelVendorExtensionData),
                        requiredAnnotationArg("x-other-field-name", modelVendorExtensionData),
                        requiredAnnotationArg("x-has-any-value-of", modelVendorExtensionData),
                        nonRequiredAnnotationArg("x-message", modelVendorExtensionData)
                    ));
            }
        );

    @Override
    public Map<String, Object> postProcessModels(Map<String, Object> objs) {
        List<Object> models = (List<Object>) objs.get("models");
        models.forEach(
            modelEntry -> {
                CodegenModel codegenModel = (CodegenModel) ((Map<String, Object>) modelEntry).get("model");
                Map<String, Object> vendorExtensions = codegenModel.getVendorExtensions();
                List<String> annotationsToAdd = new ArrayList<>();
                for (String vendorExtensionKey : vendorExtensions.keySet()) {
                    Optional.ofNullable(
                        ANNOTATION_CREATOR_BY_TAG.get(vendorExtensionKey)
                    ).ifPresent(codegenModelUpdater -> codegenModelUpdater.accept(new ModelVendorExtensionData(
                        objs.get("imports"),
                        annotationsToAdd,
                        vendorExtensions.get(vendorExtensionKey), vendorExtensionKey
                    )));
                }

                if (!annotationsToAdd.isEmpty()) {
                    vendorExtensions.put("x-annotations-to-add", annotationsToAdd.stream()
                        .map(annotationToAdd -> Map.of("annotation-to-add", annotationToAdd))
                    .collect(Collectors.toList()));
                }
            }
        );

        return super.postProcessModels(objs);
    }

    private static class ModelVendorExtensionData {

        private final List<Map<String, String>> imports;
        private final List<String> annotationsToAdd;
        private final Object tagChildData;
        private final String tagName;

        public ModelVendorExtensionData(Object imports, List<String> annotationsToAdd, Object tagChildData, String tagName) {
            this.imports = (List<Map<String, String>>) imports;
            this.annotationsToAdd = annotationsToAdd;
            this.tagChildData = tagChildData;
            this.tagName = tagName;
        }

        public List<String> getAnnotationsToAdd() {
            return annotationsToAdd;
        }

        public Object getTagChildData() {
            return tagChildData;
        }

        public String getTagName() {
            return tagName;
        }

        public void addAnnotation(String annotationPackage, String annotationName, List<AnnotationArg> annotationArgs) {
            getAnnotationsToAdd().add(
                String.format("@%s(%s)", annotationName, createAnnotationArgs(annotationArgs))
            );
            imports.add(Map.of("import", String.format("%s.%s", annotationPackage, annotationName)));
        }
    }

    private static Object getRequiredFieldValue(ModelVendorExtensionData modelVendorExtensionData, String fieldName) {
        Map<String, Object> childTagMap = (Map<String, Object>) modelVendorExtensionData.getTagChildData();
        return Optional.ofNullable(childTagMap.get(fieldName))
            .orElseThrow(() -> new IllegalArgumentException(
                String.format("cannot find required field: '%s' under tag: '%s'", fieldName, modelVendorExtensionData.getTagName())));
    }

    private static Object getNonRequiredFieldValue(ModelVendorExtensionData modelVendorExtensionData, String fieldName) {
        Map<String, Object> childTagMap = (Map<String, Object>) modelVendorExtensionData.getTagChildData();
        return childTagMap.get(fieldName);
    }

    private static String createAnnotationArgs(List<AnnotationArg> annotationArgs) {
        return annotationArgs.stream()
            .filter(entry -> Objects.nonNull(entry.getAnnotationValue()))
            .map(entry -> String.format("%s = %s", entry.getJavaArgName(), formatAnnotationArgByType(entry.getAnnotationValue())))
            .collect(Collectors.joining(", "));
    }

    private static String formatAnnotationArgByType(Object argValue) {
        if (argValue instanceof String) {
            return String.format("\"%s\"", argValue.toString());
        }
        if (argValue instanceof List) {
            String arrayArgsAsText = ((List<Object>) argValue).stream()
                .map(entry -> String.format("\"%s\"", entry.toString()))
                .collect(Collectors.joining(", "));
            return String.format("{%s}", arrayArgsAsText);
        }
        throw new IllegalArgumentException(String.format("not supported type %s for annotation argument", argValue.getClass().getCanonicalName()));
    }

    private static AnnotationArg requiredAnnotationArg(String tagName, ModelVendorExtensionData modelVendorExtensionData) {
        return new AnnotationArg(tagName, getRequiredFieldValue(modelVendorExtensionData, tagName));
    }

    private static AnnotationArg nonRequiredAnnotationArg(String tagName, ModelVendorExtensionData modelVendorExtensionData) {
        return new AnnotationArg(tagName, getNonRequiredFieldValue(modelVendorExtensionData, tagName));
    }

    private static class AnnotationArg {

        private final String tagName;
        private final Object annotationValue;

        public AnnotationArg(String tagName, Object annotationValue) {
            this.tagName = tagName;
            this.annotationValue = annotationValue;
        }

        public String getTagName() {
            return tagName;
        }

        public Object getAnnotationValue() {
            return annotationValue;
        }

        public String getJavaArgName() {
            return CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, tagName.replace("x-", ""));
        }
    }
}
