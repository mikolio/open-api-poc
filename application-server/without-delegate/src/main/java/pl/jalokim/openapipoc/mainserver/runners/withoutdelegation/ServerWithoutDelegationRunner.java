package pl.jalokim.openapipoc.mainserver.runners.withoutdelegation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pl.jalokim.openapipoc.mainserver.pet.common.AppConfiguration;

@SpringBootApplication
@Import(AppConfiguration.class)
public class ServerWithoutDelegationRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServerWithoutDelegationRunner.class, args);
    }

}
