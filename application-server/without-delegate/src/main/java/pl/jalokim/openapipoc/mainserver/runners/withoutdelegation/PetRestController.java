package pl.jalokim.openapipoc.mainserver.runners.withoutdelegation;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.jalokim.application.api.feign.pet.PetApi;
import pl.jalokim.application.model.pet.CategoryQuery;
import pl.jalokim.application.model.pet.ModelApiResponse;
import pl.jalokim.application.model.pet.Pet;
import pl.jalokim.application.model.pet.PetQuery;
import pl.jalokim.application.model.pet.TagQuery;
import pl.jalokim.openapipoc.mainserver.pet.common.PetDummyService;
import pl.jalokim.page.PagePet;

@RestController
@RequiredArgsConstructor
public class PetRestController implements PetApi {

    private final PetDummyService petDummyService;

    public ResponseEntity<Void> addPet(@Valid @RequestBody Pet pet) {
        return petDummyService.addPet(pet);
    }

    public ResponseEntity<Void> deletePet(Long aLong, String s) {
        return null;
    }

    public ResponseEntity<List<Pet>> findByObjects(@NotNull @Valid TagQuery tagQuery, @NotNull @Valid CategoryQuery categoryQuery) {
        return petDummyService.findByObjects(tagQuery, categoryQuery);
    }

    public ResponseEntity<List<Pet>> findByPetQuery(@Valid PetQuery petQuery) {
        return petDummyService.findByPetQuery(petQuery);
    }

    public ResponseEntity<List<Pet>> findBySimpleArgs(@NotNull @Valid List<String> list, @NotNull @Valid String s) {
       return petDummyService.findBySimpleArgs(list, s);
    }

    public ResponseEntity<Page<Pet>> findPageWithGenericPetTypeByPageable(@Valid PetQuery petQuery, Pageable pageable) {
        return petDummyService.findPageWithGenericPetTypeByPageable(petQuery, pageable);
    }

    public ResponseEntity<PagePet> findPetPageByOwnPageable(@Valid Pageable pageable) {
        return petDummyService.findPetPageByOwnPageable(pageable);
    }

    public ResponseEntity<List<Pet>> findPetsByStatus(@NotNull @Valid List<String> list) {
        return null;
    }

    public ResponseEntity<List<Pet>> findPetsByTags(@NotNull @Valid List<String> list, Pageable pageable) {
        return null;
    }

    public ResponseEntity<List<Pet>> findPetsWithInlinePageable(@Valid Pageable pageable) {
        return null;
    }

    public ResponseEntity<List<Pet>> findPetsWithRefPageable(@Valid Pageable pageable) {
        return null;
    }

    public ResponseEntity<Page> findRawPageByOwnPageable(@Valid Pageable pageable) {
        return petDummyService.findRawPageByOwnPageable(pageable);
    }

    public ResponseEntity<Pet> getPetById(Long aLong) {
        return null;
    }

    public ResponseEntity<Void> updatePet(@Valid Pet pet) {
        return null;
    }

    public ResponseEntity<Void> updatePetWithForm(Long aLong, String s, String s1) {
        return null;
    }

    public ResponseEntity<ModelApiResponse> uploadFile(Long aLong, String s, MultipartFile multipartFile) {
        return null;
    }
}
