# Pet api server implementation

This project contains example server side api code based on pet api.
Server starts on 8088 port. 

To start application run one of below class:

- ServerWithDelegationRunner (from with-delegate module) - this is example of usage delegate pattern for api, this not contains Rest Controller classes, 
  them comes from  pl.jalokim.openapipoc:server-stub-with-delegate-pattern
- ServerWithoutDelegationRunner (from without-delegate) - this is example of usage normal implementation of api interface.
  This module contains Rest Controller class provided explicitly. 

## swagger 
swagger under link http://localhost:8088/swagger-ui.html#/ 
