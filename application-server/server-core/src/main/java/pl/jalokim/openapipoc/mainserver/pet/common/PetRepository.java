package pl.jalokim.openapipoc.mainserver.pet.common;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<PetEntity, Long> {

}
