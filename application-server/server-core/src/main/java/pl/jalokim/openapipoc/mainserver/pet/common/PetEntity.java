package pl.jalokim.openapipoc.mainserver.pet.common;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;
import pl.jalokim.application.model.pet.Pet;

@Entity
@Data
public class PetEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String categoryName;
    private Pet.StatusEnum statusEnum;
}
