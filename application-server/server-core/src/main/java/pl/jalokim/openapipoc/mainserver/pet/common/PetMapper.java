package pl.jalokim.openapipoc.mainserver.pet.common;

import java.util.Optional;
import org.springframework.stereotype.Component;
import pl.jalokim.application.model.pet.Category;
import pl.jalokim.application.model.pet.Pet;

@Component
public class PetMapper {

    public Pet toDto(PetEntity petEntity) {
        Pet pet = new Pet();
        pet.setId(petEntity.getId());
        pet.setName(petEntity.getName());
        pet.setCategory(createCategory(petEntity.getCategoryName()));
        pet.setStatus(petEntity.getStatusEnum());
        return pet;
    }

    public PetEntity toEntity(Pet petDto) {
        PetEntity pet = new PetEntity();
        pet.setId(petDto.getId());
        pet.setName(petDto.getName());
        pet.setStatusEnum(petDto.getStatus());
        pet.setCategoryName(Optional.ofNullable(
            petDto.getCategory()
        ).map(Category::getName)
            .orElse(null));
        return pet;
    }

     public Category createCategory(String name) {
        Category category = new Category();
        category.setName(name);
        return category;
    }
}
