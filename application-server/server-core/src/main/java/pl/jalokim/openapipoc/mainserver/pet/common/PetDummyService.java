package pl.jalokim.openapipoc.mainserver.pet.common;

import io.micrometer.core.instrument.util.StringUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.jalokim.application.model.pet.CategoryQuery;
import pl.jalokim.application.model.pet.Pet;
import pl.jalokim.application.model.pet.PetQuery;
import pl.jalokim.application.model.pet.TagQuery;
import pl.jalokim.page.PagePet;

@Service
@RequiredArgsConstructor
public class PetDummyService {

    private final PetRepository petRepository;
    private final PetMapper petMapper;

    public ResponseEntity<Void> addPet(Pet pet) {
        petRepository.save(petMapper.toEntity(pet));
        return ResponseEntity.noContent().build();
    }

    public ResponseEntity<List<Pet>> findByObjects(TagQuery tagQuery, CategoryQuery categoryQuery) {
        return ResponseEntity.ok(
            petRepository.findAll().stream()
                .map(petMapper::toDto)
                .collect(Collectors.toUnmodifiableList())
        );
    }

    public ResponseEntity<List<Pet>> findByPetQuery(PetQuery petQuery) {
        return ResponseEntity.ok(
            petRepository.findAll().stream()
                .filter(petEntity -> petEntity.getName().contains(petQuery.getName())
                    && (StringUtils.isBlank(petQuery.getStatusName()) || containsCategory(petEntity, petQuery)))
                .map(petMapper::toDto)
                .collect(Collectors.toUnmodifiableList())
        );
    }

    private boolean containsCategory(PetEntity petEntity, PetQuery petQuery) {
        return Optional.ofNullable(petEntity.getStatusEnum())
            .map(Enum::name)
            .map(String::toLowerCase)
            .map(s -> s.contains(petQuery.getStatusName().toLowerCase()))
            .orElse(true);
    }

    public ResponseEntity<List<Pet>> findBySimpleArgs(List<String> list, String s) {
        return ResponseEntity.ok(
            petRepository.findAll().stream()
                .map(petMapper::toDto)
                .collect(Collectors.toUnmodifiableList())
        );
    }

    public ResponseEntity<Page<Pet>> findPageWithGenericPetTypeByPageable(PetQuery petQuery, Pageable pageable) {
        return ResponseEntity.ok(petRepository.findAll(pageable)
            .map(petMapper::toDto));
    }

    public ResponseEntity<PagePet> findPetPageByOwnPageable(Pageable pageable) {
        Page<Pet> pageOfPets = petRepository.findAll(pageable)
            .map(petMapper::toDto);

        return ResponseEntity.ok(new PagePet(pageOfPets.getContent(), pageable, pageOfPets.getTotalElements()));
    }

    public ResponseEntity<Page> findRawPageByOwnPageable(Pageable pageable) {
        return ResponseEntity.ok(petRepository.findAll(pageable)
            .map(petMapper::toDto));
    }
}
