package pl.jalokim.openapipoc.mainserver.runners.withdelagation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import pl.jalokim.openapipoc.mainserver.pet.common.AppConfiguration;

@SpringBootApplication
@Import(AppConfiguration.class)
@ComponentScan("pl.jalokim.application.api.pet")
@ComponentScan("pl.jalokim.openapipoc.mainserver.runners.withdelagation")
public class ServerWithDelegationRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServerWithDelegationRunner.class, args);
    }

}
