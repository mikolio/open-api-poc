package pl.jalokim.openapipoc.mainserver.runners.withdelagation;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.jalokim.application.api.pet.PetApiDelegate;
import pl.jalokim.application.model.pet.CategoryQuery;
import pl.jalokim.application.model.pet.Pet;
import pl.jalokim.application.model.pet.PetQuery;
import pl.jalokim.application.model.pet.TagQuery;
import pl.jalokim.openapipoc.mainserver.pet.common.PetDummyService;
import pl.jalokim.page.PagePet;

/**
 * This class is injected in class {@link pl.jalokim.application.api.pet.PetApiController}
 */
@Service
@RequiredArgsConstructor
public class PetApiDelegateImpl implements PetApiDelegate {

    private final PetDummyService petDummyService;

    @Override
    public ResponseEntity<Void> addPet(Pet pet) {
        return petDummyService.addPet(pet);
    }

    @Override
    public ResponseEntity<List<Pet>> findByObjects(TagQuery tagQuery, CategoryQuery categoryQuery) {
        return petDummyService.findByObjects(tagQuery, categoryQuery);
    }

    @Override
    public ResponseEntity<List<Pet>> findByPetQuery(PetQuery petQuery) {
        return petDummyService.findByPetQuery(petQuery);
    }

    @Override
    public ResponseEntity<List<Pet>> findBySimpleArgs(List<String> list, String s) {
        return petDummyService.findBySimpleArgs(list, s);
    }

    @Override
    public ResponseEntity<Page<Pet>> findPageWithGenericPetTypeByPageable(PetQuery petQuery, Pageable pageable) {
        return petDummyService.findPageWithGenericPetTypeByPageable(petQuery, pageable);
    }

//    @Override
    public ResponseEntity<PagePet> findPetPageByOwnPageable(Pageable pageable) {
        return petDummyService.findPetPageByOwnPageable(pageable);
    }

//    @Override
    public ResponseEntity<Page> findRawPageByOwnPageable(Pageable pageable) {
        return petDummyService.findRawPageByOwnPageable(pageable);
    }
}
