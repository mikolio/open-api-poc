import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BASE_PATH} from 'pet-client-rest';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PetClientApiModule} from 'pet-client-rest';
import {HttpClientModule} from '@angular/common/http';
import {AddPetComponent} from './add-pet/add-pet.component';
import {PetByObjectQueryComponent} from './pet-by-object-query/pet-by-object-query.component';
import {PageablePetByObjectQueryComponent} from "./pageable-pet-by-object-query/pageable-pet-by-object-query.component";

@NgModule({
  declarations: [
    AppComponent,
    AddPetComponent,
    PetByObjectQueryComponent,
    PageablePetByObjectQueryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PetClientApiModule,
    HttpClientModule
  ],
  providers: [{provide: BASE_PATH, useValue: '/api'}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
