import {Component} from '@angular/core';

import {Category, OwnPageable, Page, Pet} from 'pet-client-rest';
import {PetService} from 'pet-client-rest';
import {PetQuery} from "pet-client-rest/model/models";

@Component({
  selector: 'pet-by-object-query',
  templateUrl: './pet-by-object-query.component.html',
  styleUrls: ['./pet-by-object-query.component.css']
})
export class PetByObjectQueryComponent {

  constructor(private petService: PetService) {
  }

  ngOnInit() {
  }

  petQueryModel = {name: '', tagName: 'sample', statusName: ''} as PetQuery;

  allPets: Pet[] = [];

  errorMessage: any;
  hasError = false as boolean;

  onSubmit() {
    this.getPets();
  }

  get diagnostic() {
    return JSON.stringify(this.petQueryModel);
  }


  getPets(): void {
    this.petService.findByPetQuery(this.petQueryModel)
      .subscribe(pets => {
        this.allPets = pets;
        this.hasError = false;
      }, error => {
        this.hasError = true;
        this.errorMessage = JSON.stringify(error)
      });
  }
}
