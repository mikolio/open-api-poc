import {Component} from '@angular/core';

import {Category, OwnPageable, Page, Pet} from 'pet-client-rest';
import {PetService} from 'pet-client-rest';
import {PetQuery} from "pet-client-rest/model/models";

@Component({
  selector: 'pageable-pet-by-object-query',
  templateUrl: './pageable-pet-by-object-query.component.html',
  styleUrls: ['./pageable-pet-by-object-query.component.css']
})
export class PageablePetByObjectQueryComponent {

  constructor(private petService: PetService) {
  }

  ngOnInit() {
  }

  pageablePetQueryModel = {name: '', tagName: 'sample', statusName: ''} as PetQuery;
  pageable = {page: 0, size: 5} as OwnPageable;

  pagedPets: Pet[] = [];
  currentPetPage = {} as Page<Pet>;

  errorMessage: any;
  hasError = false as boolean;


  onPagedSubmit() {
    this.getPagedPets()
  }

  get diagnostic() {
    return JSON.stringify(this.pageablePetQueryModel);
  }

  getPagedPets(): void {
    this.petService.findPageWithGenericPetTypeByPageable(this.pageablePetQueryModel, this.pageable)
      .subscribe(petsPage => {
          this.pagedPets = petsPage.content;
          this.currentPetPage = petsPage;
          this.hasError = false;
        }, error => {
          this.hasError = true;
          this.errorMessage = JSON.stringify(error)
        });
  }
}
