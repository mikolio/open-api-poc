import {Component} from '@angular/core';

import {Category, Pet} from 'pet-client-rest';
import {PetService} from 'pet-client-rest';
import StatusEnum = Pet.StatusEnum;

@Component({
  selector: 'add-pet-form',
  templateUrl: './add-pet.component.html',
  styleUrls: ['./add-pet.component.css']
})
export class AddPetComponent {

  constructor(private petService: PetService) {
  }

  addPetModel = {id: 15, category: {}, name: 'some name', photoUrls: ['url1', 'url2']} as Pet;

  selectedCategoryId;

  errorMessage: any;
  hasError = false as boolean;

  categories = [
    {id: 1, name: 'category 1'} as Category,
    {id: 2, name: 'category 2'} as Category,
    {id: 3, name: 'category 3'} as Category
  ] as Array<Category>;

  statuses = [StatusEnum.Available, StatusEnum.Pending, StatusEnum.Sold] as Array<Pet.StatusEnum>;

  submitted = false;

  onSubmit() {
    this.addPetModel.category = this.categories.find(element => this.selectedCategoryId == element.id)
    this.petService.addPet(this.addPetModel)
      .subscribe(result => {
          console.log(result);
          this.submitted = true;
          this.hasError = false;
        }, error => {
          this.hasError = true;
          this.errorMessage = JSON.stringify(error)
        })
  }

  get diagnostic() {
    return JSON.stringify(this.addPetModel);
  }

}
