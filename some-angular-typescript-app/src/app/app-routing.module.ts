import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddPetComponent} from "./add-pet/add-pet.component";
import {PetByObjectQueryComponent} from "./pet-by-object-query/pet-by-object-query.component";
import {PageablePetByObjectQueryComponent} from "./pageable-pet-by-object-query/pageable-pet-by-object-query.component";

const routes: Routes = [
  { path: 'add-pet', component: AddPetComponent },
  { path: 'pet-by-query', component: PetByObjectQueryComponent },
  { path: 'pageable-pet-by-object-query', component: PageablePetByObjectQueryComponent },
  { path: '', component: PetByObjectQueryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
