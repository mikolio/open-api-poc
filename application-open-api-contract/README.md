# Project with example Open Api contract and code generation

## Build project

Before build project you need have installed (for generate front-end api client code):

- nodejs and npm from https://nodejs.org/en/download/
- verdaccio from https://verdaccio.org/docs/en/installation
  just install by `npm install -g verdaccio`
- change default local npm registry to by `npm set registry http://localhost:4873/`
- login to npm `npm login` setup login and password
- run verdaccio by just call in terminal or cmd `verdaccio`
- build application by maven `mvn clean install`

to generate code without front-end api client run just `mvn clean install -Pskip-frontend-build`

## Modules
- java-models - it generates api models in java
- pet-api-feign-client - it generates java spring feign client for api
- server-stub-with-delegate-pattern - it generates server stub code with delegation pattern. 
- typescript-angular-client-generation - it generates angular-typescript api client code 


## Example contracts
- pet-api.yaml in located in `src\main\resources\api-contract\pet-api.yaml`
- store-api.yaml in located in `src\main\resources\api-contract\store-api.yaml`
