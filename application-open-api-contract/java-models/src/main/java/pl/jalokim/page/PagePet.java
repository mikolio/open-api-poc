package pl.jalokim.page;

import java.util.List;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import pl.jalokim.application.model.pet.Pet;

public class PagePet extends PageImpl<Pet> {

    public PagePet(List<Pet> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public PagePet(List<Pet> content) {
        super(content);
    }
}
