package pl.jalokim.annotations.extgenerator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FieldRequiredWhenOtherEmpty {

    String requiredFieldName();
    String emptyFieldName();
    String message() default "Field {requiredFieldName} required when field {emptyFieldName} is empty";
}
