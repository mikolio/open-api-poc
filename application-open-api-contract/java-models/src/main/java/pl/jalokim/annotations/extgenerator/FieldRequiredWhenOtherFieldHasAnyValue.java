package pl.jalokim.annotations.extgenerator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FieldRequiredWhenOtherFieldHasAnyValue {

    String requiredFieldName();
    String otherFieldName();
    String[] hasAnyValueOf();
    String message() default "Field {requiredFieldName} required when field {otherFieldName} has any value of {hasAnyValueOf}";
}
