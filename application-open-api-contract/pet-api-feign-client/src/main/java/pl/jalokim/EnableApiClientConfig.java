package pl.jalokim;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("pl.jalokim.application.api.feign")
@EnableFeignClients
public class EnableApiClientConfig {

}
