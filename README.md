# Open API POC

This POC contains few projects:

- extended-spring-codegen - this is example of extension open api code generator. 
- application-open-api-contract - it generates code from open api contract. It generates model classes for api in java, api interfaces, server stubs, java client for api via spring feign and angular-typescript api client. 
- application-server - this is example of api implementation. This contains example of delegate pattern (without Rest controller classes) and with normally explicitly provided rest controller classes. 
- java-application-client - this is example of usage generated java spring feign api client. 
- some-angular-typescript-app - this is some angular-typescript application which call api server. 

## how to start
- firstly build `extended-spring-codegen` project
- next build `application-open-api-contract` project (more you have in application-open-api-contract project readme)
- next start server in `application-server` project 
- run front-end app `some-angular-typescript-app` and java client application in `java-application-client project`. 
