package pl.jalokim.pet.client;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.jalokim.application.api.feign.pet.PetApiClient;
import pl.jalokim.application.model.pet.Pet;
import pl.jalokim.application.model.pet.PetQuery;

@RestController
@RequiredArgsConstructor
public class PetClientRestController {

    private final PetApiClient petApiClient;

    @GetMapping("pet-client/findPageWithGenericPetTypeByPageable")
    public Page<Pet> getPets(PetQuery petQuery, Pageable pageable) {
        return petApiClient.findPageWithGenericPetTypeByPageable(petQuery, pageable).getBody();
    }
}
