package pl.jalokim.pet.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.support.PageJacksonModule;
import org.springframework.context.annotation.Bean;
import com.fasterxml.jackson.databind.Module;
import org.springframework.context.annotation.Import;
import pl.jalokim.EnableApiClientConfig;

@SpringBootApplication
@Import(EnableApiClientConfig.class)
@EnableFeignClients
public class MainApplicationClientRunner {

    public static void main(String[] args) {
        SpringApplication.run(MainApplicationClientRunner.class, args);
    }

    @Bean
    public Module pageJacksonModule() {
        return new PageJacksonModule();
    }
}
