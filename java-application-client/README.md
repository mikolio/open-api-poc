# Pet api server implementation

This project contains example java via spring feign consumer of pet api
server starts on 8081 port. It invokes http request to server on localhost:8088 which implements pet api.  

Runner class `MainApplicationClientRunner`

after run hit URL `http://localhost:8081/pet-client/findPageWithGenericPetTypeByPageable?page=0&size=10`
This URL will invoke GET at `http://localhost:8088/pet/findPageWithGenericPetTypeByPageable?page=0&size=10`
